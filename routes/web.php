<?php

use App\Http\Controllers\AsesorController;
use App\Http\Controllers\CajeroController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'home')->name('home');


Route::view('/cajero/retiro', 'cajero/retirar')->name('cajero.retiro');
Route::view('/cajero/notificacion', 'cajero/mensaje')->name('cajero.notificacion');
Route::patch('/cajero/retiro', [CajeroController::class, 'update'])->name('cajero.retirar');


Route::view('/asesor/nueva', 'asesor/cuenta')->name('asesor');
Route::post('/asesor/nueva', [AsesorController::class, 'store'])->name('asesor.nueva');

Route::view('/asesor/home', 'asesor/home')->name('asesor.home');

Route::view('/asesor/notificacion', 'asesor/mensaje')->name('asesor.notificacion');
Route::post('/asesor/notificacion', [AsesorController::class, 'show'])->name('asesor.show');



//
