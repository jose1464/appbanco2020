@extends('plantilla')

@section('title', 'Notificaciones')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
        </div>
        <div class="">

            <div class="card text-center">
                <div class="card-header">
                    <h1>Notificacion</h1>
                </div>
                <div class="card-body">
                   {{$mensajje}}
                </div>
                <div class="card-footer text-muted">
                    <a href={{route('asesor.home')}}>Regresar...</a>
                </div>
            </div>
        </div>
        <div class="col">
        </div>
    </div>
</div>
@endsection