@extends('plantilla')

@section('title', 'Notificaciones')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
        </div>
        <div class="">

            <div class="card text-center">
                <div class="card-header">
                    <h1>Bienvenido Asesor</h1>
                </div>
                <div class="card-body">
                    <form action={{route('asesor.show')}} method="post">
                        @csrf
                        <div class="form-group">
                            <label for="numeroCuenta">Numero de Cuenta</label>
                            <input type="number" class="form-control" id="numeroCuenta" name="numeroCuenta" placeholder="Ingrese el numero de la cuenta" required>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Consultar saldo</button>
                    </form>
                </div>
                <div class="card-footer text-muted">
                    <a href="{{route('asesor.nueva')}}" class="btn btn-primary btn-lg btn-block active" role="button" aria-pressed="true">Crear Cuenta</a>
                </div>
            </div>
        </div>
        <div class="col">
        </div>
    </div>
</div>
@endsection