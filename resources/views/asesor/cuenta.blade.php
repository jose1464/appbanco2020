@extends('plantilla')

@section('title', 'Asesor')

@section('content')
<div class="container ">
    <div class="row">
        <div class="col">
        </div>
        <div class="col md-5">
            <div class="card text-center">
                <div class="card-header">
                    <h1>Crear Cuenta</h1>
                </div>
                <div class="card-body ">
                    <form action={{route('asesor.nueva',)}} method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nombre completo del titular</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="numeroCuenta">Numero de Cuenta</label>
                            <input type="number" class="form-control" id="numeroCuenta" name="numeroCuenta" required>
                        </div>
                        <div class="form-group">
                            <label for="saldoInicial">Saldo Inicial</label>
                            <input type="number" class="form-control" id="saldoInicial" name="saldoInicial" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Correo</label>
                            <input type="email" class="form-control" id="email" name="email" required>
                        </div>
                        <div class="form-group">
                            <label for="contraseña">Contraseña</label>
                            <input type="password" class="form-control" id="contraseña" name="contraseña" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Registrar</button>
                    </form>
                </div>
                <div class="card-footer text-muted">
                    <a href="{{route('asesor.home')}}">Regresar...</a>
                </div>
            </div>
        </div>
        <div class="col">
        </div>
    </div>
</div>
@endsection