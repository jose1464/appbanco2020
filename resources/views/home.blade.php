@extends('plantilla')

@section('title', 'Notificaciones')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
        </div>
        <div class="">

            <div class="card text-center">
                <div class="card-header">
                    <h1>Bienvenido...</h1>
                </div>
                <div class="card-body">
                    <a href="{{route('asesor.nueva')}}" class="btn btn-primary btn-lg btn-block active" role="button" aria-pressed="true">Ingresa como asesor</a>
                    <a href="{{route('cajero.retiro')}}" class="btn btn-primary btn-lg btn-block active" role="button" aria-pressed="true">Retirar dinero</a>
                </div>
                <div class="card-footer text-muted">
                </div>
            </div>
        </div>
        <div class="col">
        </div>
    </div>
</div>
@endsection