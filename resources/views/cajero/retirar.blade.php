@extends('plantilla')

@section('title', 'Inicio')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
        </div>
        <div class="">

            <div class="card text-center">
                <div class="card-header">
                    <h1>Retirar Dinero</h1>
                </div>
                <div class="card-body">
                    <form action={{route('cajero.retirar',)}} method="post">
                        @csrf @method('patch')
                        <div class="form-group">
                            <label for="titular">Titular</label>
                            <input type="text" class="form-control" id="titular" name="titular" required>
                        </div>
                        <div class="form-group">
                            <label for="numerocuenta">Numero de cuenta</label>
                            <input type="number" class="form-control" id="numerocuenta" name="numerocuenta" required>
                        </div>
                        <div class="form-group">
                            <label for="monto">Monto a retirar</label>
                            <input type="number" class="form-control" id="monto" name="monto" required>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Retirar</button>
                    </form>
                </div>
                <div class="card-footer text-muted">
                    
                </div>
            </div>
        </div>
        <div class="col">
        </div>
    </div>
</div>
@endsection