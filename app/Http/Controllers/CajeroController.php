<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isEmpty;

class CajeroController extends Controller
{
    public function index()
    {
        //
    }

    public function update()
    {
        $mensaje = "";
        $usuario = Usuario::where('cuenta', request('numerocuenta'))->first();
        if (empty($usuario)) {
            $mensaje = "Cuenta no valida";
        } else {
            $saldo = $usuario->saldo;
            $monto = request('monto');
            $numCuenta = request('numerocuenta');
            if ($monto <= $saldo && $monto > 0) {
                $nuevoSaldo =  $saldo - $monto;
                $mensaje = "El nuevo saldo de la cuenta " . $numCuenta . " es: $" . $nuevoSaldo;
                $usuario->saldo = $nuevoSaldo;
                $usuario->save();
            } else {
                $mensaje = "Monto no valido";
            }
        }
        return view('/cajero/mensaje')->with('mensajje', $mensaje);
    }
}
