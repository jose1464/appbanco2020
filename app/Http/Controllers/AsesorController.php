<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;

class AsesorController extends Controller
{


    public function show()
    {
        $usuario = Usuario::where('cuenta', request('numeroCuenta'))->first();
        if (empty($usuario)) {
            $mensaje = "Cuenta no existe";
        } else {
            $mensaje = "El saldo es: " . $usuario->saldo;
        }
        return view('/asesor/mensaje')->with('mensajje', $mensaje);
    }



    public function store()
    {
        $mensaje = "";
        //$usuario = Usuario::where('numeroCuenta', request('numerocuenta')->orWhere('email', request('email')))->first();//
        //$usuario = Usuario::whereCuentaOrEmail(request('numeroCuenta'), request('email'));

        //$usuario = Usuario::whereCuentaOrEmail(request('numeroCuenta'), request('email'))->first();
        $usuario = Usuario::where('cuenta', request('numeroCuenta'))->orWhere('email', request('email'))->first();
        if (empty($usuario)) {
            $user = new Usuario;
            $user->name = request('name');
            $user->email = request('email');
            $user->password = request('contraseña');
            $user->cuenta = request('numeroCuenta');
            $user->saldo = request('saldoInicial');
            $user->save();
            $mensaje = "Cuenta creada con exito";
        } else {
            $mensaje = "La cuenta" . request('numeroCuenta') . " o el correo " . request('email') . " ya existe";
        }
        return view('/asesor/mensaje')->with('mensajje', $mensaje);
    }
}
